class AddTextImages < ActiveRecord::Migration[5.2]
  def change
    create_table('text_images') do |t|
      t.string :text
      t.string :url
    end
  end
end
