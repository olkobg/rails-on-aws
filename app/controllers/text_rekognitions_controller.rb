class TextRekognitionsController < ApplicationController

  def index
    @text_images = TextImage.all
  end

  def show
  end

  def new
  end

  def create
    text_image = TextImage.create(text_image_params)
    text_image_params[:image]
    text_image.save
    redirect_to text_rekognition_path
  end

  def update
  end

  private

  def text_image_params
    params.require(:text_image).permit(:image)
  end
end
