Rails.application.routes.draw do
  root 'home#show'
  # get 'home#show'

  resources :known_face_rekognitions

  resources :face_rekognitions

  resources :text_rekognitions

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
