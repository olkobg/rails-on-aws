require 'test_helper'

class KnownFaceRekognitionsControllerTest < ActionDispatch::IntegrationTest
  test 'should get new' do
    get new_known_face_rekognition_url
    assert_response :success
  end

  test 'should post create' do
    post known_face_rekognition_url
    assert_response :success
  end

  test 'should index' do
    get known_face_rekognition_url
    assert_response :success
  end

  test 'should patch update' do
    patch known_face_rekognition_url
    assert_response :success
  end

  test 'should delete' do
    delete known_face_rekognition_url
    assert_response :success
  end

end
