require 'test_helper'

class FaceRekognitionsControllerTest < ActionDispatch::IntegrationTest
  test 'should get show' do
    get face_rekognition_url
    assert_response :success
  end

  test 'should get new' do
    get new_face_rekognition_url
    assert_response :success
  end

  test 'should get create' do
    post face_rekognition_url
    assert_response :success
  end

  test 'should get update' do
    patch face_rekognition_url
    assert_response :success
  end

end
