require 'test_helper'

class TextRekognitionsControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get text_rekognition_url
    assert_response :success
  end

  test "should get new" do
    get new_text_rekognition_url
    assert_response :success
  end

  test "should get create" do
    post text_rekognition_url
    assert_response :success
  end

  test "should get update" do
    patch text_rekognition_url
    assert_response :success
  end

end
