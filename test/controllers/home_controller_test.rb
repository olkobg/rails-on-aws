require 'test_helper'

  class HomeControllerTest < ActionDispatch::IntegrationTest
    test 'should get show' do
      get root_url
      assert_response :success
    end

  test 'should have button face rekognition' do
    get root_url
    assert_response :success
    assert_select   'a', 'face'
    assert_select   "a[href='#{face_rekognition_path}']"
  end

  test 'should have button known face rekognition' do
    get root_url
    assert_response :success
    assert_select   'a', 'known face'
    assert_select   "a[href='#{known_face_rekognition_path}']"
  end

  test 'should have button text rekognition' do
    get root_url
    assert_response :success
    assert_select   'a', 'text'
    assert_select   "a[href='#{text_rekognition_path}']"
  end


end
